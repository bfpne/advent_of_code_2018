def distance_between(x1, y1, x2, y2):
    return abs(x2 - x1) + abs(y1 - y2)

def read_file(filename):
    coordinates = set()
    with open(filename, 'r') as f:
        for l in f.readlines():
            x, y = l.strip().split(',')
            x = int(x)
            y = int(y)
            coordinates.add((x,y))

    return coordinates

def edges(coordinates):
    min_x = min(coordinates, key=lambda c: c[0])[0]
    min_y = min(coordinates, key=lambda c: c[1])[1]
    max_x = max(coordinates, key=lambda c: c[0])[0]
    max_y = max(coordinates, key=lambda c: c[1])[1]
    return min_x, min_y, max_x, max_y


def new_coordinates_in_expanded_area(orig_x, orig_y, distance):
    new_coordinates = []
    for x in range(orig_x - distance, orig_x + distance + 1):
        new_coordinates.append((x, orig_y + distance))
        new_coordinates.append((x, orig_y - distance))
    for y in range(orig_y - distance + 1, orig_y + distance):
        new_coordinates.append((orig_x + distance, y))
        new_coordinates.append((orig_x - distance, y))

    return new_coordinates

def size_of_largest_area(coordinates):
    min_x, min_y, max_x, max_y = edges(coordinates)

    def some_point_inside_edges(x, y, distance):
        return (x - distance) >= min_x or \
                (x + distance) <= max_x or \
                (y - distance) >= min_y or \
                (y + distance) <= max_y

    def inside_edges(x, y):
        return x >= min_x and x <= max_x and y >= min_y and y <= max_y

    def calculate_area(x, y):
        def is_closest_to_me(target_x, target_y):
            my_distance = distance_between(x, y, target_x, target_y)
            for other_x, other_y in coordinates:
                if other_x == x and other_y == y:
                    pass  # Don't compare with myself
                else:
                    if distance_between(other_x, other_y, target_x, target_y) <= my_distance:
                        return False
            return True

        area = 1
        current_distance = 1

        while some_point_inside_edges(x, y, current_distance):
            area_increase = 0
            new_coordinates = new_coordinates_in_expanded_area(x, y, current_distance)
            for new_x, new_y in new_coordinates:
                if is_closest_to_me(new_x, new_y):
                    if not inside_edges(new_x, new_y):
                        return 0  # Inifinate area
                    else:
                        area_increase += 1

            area += area_increase
            if area_increase == 0:
                return area

            current_distance += 1

        return area

    areas = []
    for x, y in coordinates:
        areas.append(calculate_area(x, y))

    return max(areas)

def size_of_area_closest_to_most_coordinates(coordinates):
    min_x, min_y, max_x, max_y = edges(coordinates)

    def sum_of_distances_is_less_than_max(x, y):
        total_distance = 0
        for target_x, target_y in coordinates:
            total_distance += distance_between(x, y, target_x, target_y)
            if total_distance >= 10000:
                return False
        return True

    size = 0
    for x in range(min_x, max_x + 1):
        for y in range(min_y, max_y + 1):
            if sum_of_distances_is_less_than_max(x, y):
                size += 1

    return size


def main():
    import sys
    if len(sys.argv) < 2:
        print("usage: {} FILENAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    coordinates = read_file(filename)
    print("6.1 result: {}".format(size_of_largest_area(coordinates)))
    print("6.2 result: {}".format(size_of_area_closest_to_most_coordinates(coordinates)))

if __name__ == '__main__':
    main()
