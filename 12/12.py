class State():
    def __init__(self, state_string, rules):
        self._RULES = rules
        self._pots = set()
        count = 0
        for p in state_string:
            if p == '#':
                self._pots.add(count)
            count += 1

    def _get_pot(self, index):
        if index in self._pots:
            return True
        else:
            return False

    def get_pots(self):
        first, last = self._get_range()
        return range(first, last + 1)

    def _get_pattern(self, index):
        return (self._get_pot(index - 2), self._get_pot(index - 1), self._get_pot(index), self._get_pot(index + 1), self._get_pot(index + 2))

    def __str__(self):
        def translate(state):
            if state:
                return '#'
            else:
                return '.'

        return ''.join([translate(self._get_pot(i)) for i in self.get_pots()])

    def _get_next_state(self, pot_index):
        try:
            return self._RULES[self._get_pattern(pot_index)]
        except KeyError:
            return False

    def _get_range(self):
        return min(self._pots), max(self._pots)

    def next_generation(self):
        next_state = {}
        first_pot, last_pot = self._get_range()
        for pot in range(first_pot - 1, last_pot + 3):
            result = self._get_next_state(pot)

            if result:
                next_state[pot] = True
        self._pots = next_state
        return self

    def sum_pot_indexes_with_plants(self):
        result = 0
        for index in self._pots:
            result += index
        return result


def parse_rule(line):
    pattern, result = line.split('=>')
    pattern = pattern.strip()
    result = result.strip() == '#'

    pattern = tuple([s == '#' for s in pattern])

    return pattern, result

def read_file(filename):
    rules = {}
    with open(filename, 'r') as f:
        initial_state = f.readline().split(':')[1].strip()
        _ = f.readline()
        for l in f.readlines():
            pattern, result = parse_rule(l)
            rules[pattern] = result
    return State(initial_state, rules)

def after_20_generations(state):
    for c in range(1, 21):
        #print('{}: {}'.format(c, state.next_generation()))
        state.next_generation()
    return state.sum_pot_indexes_with_plants()

def after_50000000000_generations(state):
    for c in range(1, 1001 - 20):  # We did 20 above
        state.next_generation()

    sum_after_1000 = state.sum_pot_indexes_with_plants()
    print('Sum after 1000: {}'.format(sum_after_1000))

    state.next_generation()
    difference_per_generation = state.sum_pot_indexes_with_plants() - sum_after_1000
    print('Difference per generation: {}'.format(difference_per_generation))

    return ((50000000000 - 1000) * difference_per_generation) + sum_after_1000

def main():
    import sys
    if len(sys.argv) < 2:
        print("usage: {} FILENAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    initial_state = read_file(filename)
    print("12.1 result: {}".format(after_20_generations(initial_state)))
    print("12.2 result: {}".format(after_50000000000_generations(initial_state)))

if __name__ == '__main__':
    main()
    #import cProfile
    #cProfile.run('main()')

