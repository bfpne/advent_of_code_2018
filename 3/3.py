
def parse_claim(line):
    import re
    result = re.search('^(#[0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)', line)
    return (result.group(1), (int(result.group(2)), int(result.group(3))), (int(result.group(4)), int(result.group(5))))

def read_file(filename):
    claims = []
    with open(filename, 'r') as f:
        for l in f.readlines():
            claims.append(parse_claim(l.strip()))
    return claims

def count_overlapping(claims):
    from collections import defaultdict
    
    square_inches = defaultdict(lambda: 0)

    for claim_id, start, size in claims:
        start_x, start_y = start
        width, height = size
        for x in range(start_x, start_x + width):
            for y in range(start_y, start_y + height):
                square_inches[(x,y)] += 1

    overlapping = 0
    for pos, count in square_inches.items():
        if count >= 2:
            overlapping += 1

    return overlapping

def find_independent(claims_list):
    from collections import defaultdict
    
    square_inch_count = defaultdict(lambda: 0)

    claims = {}

    for claim_id, start, size in claims_list:
        start_x, start_y = start
        width, height = size

        square_inches_in_claim = []
        for x in range(start_x, start_x + width):
            for y in range(start_y, start_y + height):
                square_inch_count[(x,y)] += 1
                square_inches_in_claim.append((x,y))
        claims[claim_id] = square_inches_in_claim

    def are_independent(square_inches):
        for square_inch in square_inches:
            if square_inch_count[square_inch] > 1:
                return False
        return True

    for claim_id, square_inches in claims.items():
        if are_independent(square_inches):
            return claim_id

def main():
    import sys
    if len(sys.argv) < 2:
        print("usage: {} FILENAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    claims = read_file(filename)
    print("3.1 result: {}".format(count_overlapping(claims)))
    print("3.2 result: {}".format(find_independent(claims)))

if __name__ == '__main__':
    main()

