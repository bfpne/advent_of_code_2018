
def read_file(filename):
    lines = []
    with open(filename, 'r') as f:
        for l in f.readlines():
            lines.append(l.strip())
    return lines

def count_letters(box_id):
    from collections import defaultdict
    occurances = defaultdict(lambda: 0)
    for c in box_id:
        occurances[c] += 1

    a_letter_appears_twice = 0
    a_letter_appears_three_times = 0

    for c, count in occurances.items():
        if count == 2:
            a_letter_appears_twice = 1
        elif count == 3:
            a_letter_appears_three_times = 1

    return a_letter_appears_twice, a_letter_appears_three_times


def checksum(box_ids):
    boxes_with_letters_appearing_twice = 0
    boxes_with_letters_appearing_three_times = 0
    for box_id in box_ids:
        two_times, three_times = count_letters(box_id)
        boxes_with_letters_appearing_twice += two_times
        boxes_with_letters_appearing_three_times += three_times

    return boxes_with_letters_appearing_twice * boxes_with_letters_appearing_three_times

def find_common(id_x, id_y):
    count = 0
    common = []
    for x, y in zip(list(id_x), list(id_y)):
        if x == y:
            common.append(x)
    return common

def find_common_letters_in_almost_similar(id_x, box_ids):
    num_chars = len(id_x)
    for id_y in box_ids:
        common_letters = find_common(id_x, id_y)
        if len(common_letters) == num_chars - 1:
            return common_letters

def common_letters(box_ids):
    for box_id in box_ids:
        common = find_common_letters_in_almost_similar(box_id, box_ids)
        if common:
            common_str = ''.join(common)
            print('{} ~ {}'.format(box_id, common_str))
            return common_str

def main():
    import sys
    if len(sys.argv) < 2:
        print("usage: {} FILENAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    box_ids = read_file(filename)
    print("2.1 result: {}".format(checksum(box_ids.copy())))
    print("2.2 result: {}".format(common_letters(box_ids.copy())))

if __name__ == '__main__':
    main()

