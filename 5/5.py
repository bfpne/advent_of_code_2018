class Unit():
    def __init__(self, unit, prev):
        self._UNIT = unit
        self._prev = prev
        self._next = None

        if self._prev:
            self._prev.set_next(self)

    def set_next(self, unit):
        self._next = unit

    def set_prev(self, unit):
        self._prev = unit

    def next(self):
        return self._next

    def prev(self):
        return self._prev

    def value(self):
        return self._UNIT

    def __str__(self):
        return self._UNIT


def string_to_polymer(polymer_string, f = lambda c: True):
    root = Unit(None, None)
    current = root
    for u in polymer_string:
        if f(u):
            current = Unit(u, current)
    return root

def read_file(filename):
    with open(filename, 'r') as f:
        return f.read().strip()

def polymer_to_string(root):
    current = root
    clist = []
    while current:
        clist.append(str(current))
        current = current.next()
    return ''.join(clist)
        

def perform_reactions(root):
    current = root.next().next()

    while current:
        prev = current.prev()
        current_value = current.value()
        prev_value = prev.value()
        if prev_value and current_value != prev_value and current_value.lower() == prev_value.lower():
            # Remove previous and current
            new_previous = prev.prev()
            next = current.next()
            new_previous.set_next(next)
            if next:
                next.set_prev(new_previous)

        current = current.next()

    remaining = polymer_to_string(root.next())
    return remaining


def find_shortest_possible(polymer_string):
    units = set()
    for c in polymer_string.lower():
        units.add(c)

    shortest = len(polymer_string)
    shortest_removed = None
    for unit in units:
        current_len = len(perform_reactions(string_to_polymer(polymer_string, lambda c: c.lower() != unit)))
        if current_len < shortest:
            shortest_removed = unit
            shortest = current_len

    return shortest


def main():
    import sys
    if len(sys.argv) < 2:
        print("usage: {} FILENAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    polymer_string = read_file(filename)
    print("5.1 result: {}".format(len(perform_reactions(string_to_polymer(polymer_string)))))
    print("5.2 result: {}".format(find_shortest_possible(polymer_string)))

if __name__ == '__main__':
    main()

