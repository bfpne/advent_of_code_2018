
def read_file(filename):
    with open(filename, 'r') as f:
        return int(f.read().strip())

def power_level(x, y, grid_serial_number):
    """
    >>> power_level(3, 5, 8)
    4
    >>> power_level(122, 79, 57)
    -5
    >>> power_level(217, 196, 39)
    0
    >>> power_level(101, 153, 71)
    4
    >>> power_level(33, 45, 18)
    4
    >>> power_level(21, 61, 42)
    4
    """
    from math import floor
    rack_id = x + 10
    power_level = rack_id * y
    power_level += grid_serial_number
    power_level *= rack_id
    return floor(power_level / 100) % 10 - 5


def sum_grid(grid, x, y, size):
    s = 0
    for x_offset in range(0, size):
        for y_offset in range(0, size):
            s += grid[(x + x_offset, y + y_offset)]
    return s

def calculate_power_levels(grid_serial_number):
    coordinates = {}

    for x in range(1, 301):
        for y in range(1, 301):
            coordinates[(x,y)] = power_level(x, y, grid_serial_number)
    return coordinates

def find_3x3_quadrant_with_most_energy(grid_serial_number):
    """
    >>> find_3x3_quadrant_with_most_energy(18)
    (33, 45)
    >>> find_3x3_quadrant_with_most_energy(42)
    (21, 61)
    """
    grid = calculate_power_levels(grid_serial_number)

    highest_sum = 0
    best_coordinate = (0, 0)
    for x in range(1, 299):
        for y in range(1, 299):
            this_sum = sum_grid(grid, x, y, 3)
            if this_sum > highest_sum:
                highest_sum = this_sum
                best_coordinate = (x, y)

    return best_coordinate


def quadrant_with_most_energy(grid_serial_number):
    grid = calculate_power_levels(grid_serial_number)

    highest_sum = 0
    best_coordinate = (0, 0)
    best_size = 0
    for size in range(1, 301):
        for x in range(1, 300 - size):
            for y in range(1, 300 - size):
                this_sum = sum_grid(grid, x, y, size)
                if this_sum > highest_sum:
                    highest_sum = this_sum
                    best_coordinate = (x, y)
                    best_size = size

    return best_coordinate, best_size


def main():
    import sys
    if len(sys.argv) < 2:
        import doctest
        doctest.testmod()
        sys.exit()

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    grid_serial_number = read_file(filename)
    print("11.1 result: {}".format(find_3x3_quadrant_with_most_energy(grid_serial_number)))
    print("11.2 result: {}".format(quadrant_with_most_energy(grid_serial_number)))

if __name__ == '__main__':
    main()

