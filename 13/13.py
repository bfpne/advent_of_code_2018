from enum import Enum, auto

class Turn(Enum):
    LEFT = auto()
    STRAIGHT = auto()
    RIGHT = auto()

    def next(self):
        if self == Turn.LEFT:
            return Turn.STRAIGHT
        elif self == Turn.STRAIGHT:
            return Turn.RIGHT
        elif self == Turn.RIGHT:
            return Turn.LEFT

class Direction(Enum):
    NORTH = auto()
    EAST = auto()
    SOUTH = auto()
    WEST = auto()

    def turn(self, direction):
        if direction == Turn.STRAIGHT:
            return self
        elif direction == Turn.LEFT:
            if self == Direction.NORTH:
                return Direction.WEST
            else:
                return Direction(self.value - 1)
        elif direction == Turn.RIGHT:
            if self == Direction.WEST:
                return Direction.NORTH
            else:
                return Direction(self.value + 1)


class Cart():
    def __init__(self, char, position):
        if char == '^':
            self._direction = Direction.NORTH
        elif char == 'v':
            self._direction = Direction.SOUTH
        elif char == '<':
            self._direction = Direction.WEST
        elif char == '>':
            self._direction = Direction.EAST
        else:
            raise RuntimeError("Invalid cart direction: '{}'".format(char))

        self._position = position
        self._next_turn = Turn.LEFT

    def __lt__(self, other):
        self_x, self_y = self._position
        other_x, other_y = other._position

        if self_y != other_y:
            return self_y < other_y
        else:
            return self_x < other_x


    def turn(self):
        self._direction = self._direction.turn(self._next_turn)
        self._next_turn = self._next_turn.next()

    def move(self, next_position, next_direction):
        self._position = next_position
        self._direction = next_direction

    @property
    def position(self):
        return self._position

    @property
    def direction(self):
        return self._direction

    def __str__(self):
        return '{}: {}'.format(self._position, self._direction)

    def __repr__(self):
        return '{}: {}'.format(self._position, self._direction)


class TrackSection():
    def __init__(self, char, position):
        self._leads_to = {}
        self._is_intersection = False
        if char == '|':
            self._leads_to[Direction.NORTH] = (position[0], position[1] - 1, Direction.NORTH)
            self._leads_to[Direction.SOUTH] = (position[0], position[1] + 1, Direction.SOUTH)
        elif char == '-':
            self._leads_to[Direction.EAST] = (position[0] + 1, position[1], Direction.EAST)
            self._leads_to[Direction.WEST] = (position[0] - 1, position[1], Direction.WEST)
        elif char == '\\':
            self._leads_to[Direction.NORTH] = (position[0] - 1, position[1], Direction.WEST)
            self._leads_to[Direction.SOUTH] = (position[0] + 1, position[1], Direction.EAST)
            self._leads_to[Direction.EAST] = (position[0], position[1] + 1, Direction.SOUTH)
            self._leads_to[Direction.WEST] = (position[0], position[1] - 1, Direction.NORTH)
        elif char == '/':
            self._leads_to[Direction.NORTH] = (position[0] + 1, position[1], Direction.EAST)
            self._leads_to[Direction.SOUTH] = (position[0] - 1, position[1], Direction.WEST)
            self._leads_to[Direction.EAST] = (position[0], position[1] - 1, Direction.NORTH)
            self._leads_to[Direction.WEST] = (position[0], position[1] + 1, Direction.SOUTH)
        elif char == '+':
            self._leads_to[Direction.NORTH] = (position[0], position[1] - 1, Direction.NORTH)
            self._leads_to[Direction.SOUTH] = (position[0], position[1] + 1, Direction.SOUTH)
            self._leads_to[Direction.EAST] = (position[0] + 1, position[1], Direction.EAST)
            self._leads_to[Direction.WEST] = (position[0] - 1, position[1], Direction.WEST)
            self._is_intersection = True
        else:
            raise RuntimeError('Invalid track section: {}'.format(char))

    @property
    def is_intersection(self):
        return self._is_intersection

    def leads_to(self, direction):
        x, y, direction = self._leads_to[direction]
        return (x, y), direction


class Track():
    def __init__(self, sections):
        self._sections = sections

    def get(self, coordinate):
        x, y = coordinate
        return self._sections[y][x]

    def size(self):
        return len(self._sections[0]), len(self._sections)


def parse_coordinate(c, pos):
    if c == ' ':
        return None, None
    elif c in set(['/', '\\', '|', '-', '+']):
        return TrackSection(c, pos), None
    else:
        cart = Cart(c, pos)
        if cart.direction == Direction.NORTH or cart.direction == Direction.SOUTH:
            return TrackSection('|', pos), cart
        else:
            return TrackSection('-', pos), cart


def read_file(filename):
    with open(filename, 'r') as f:
        track_sections = []
        carts = set()
        y = 0
        for line in f.readlines():
            x = 0
            row = []
            for c in line.strip('\n\r'):
                track_section, cart = parse_coordinate(c, (x, y))
                row.append(track_section)
                if cart:
                    carts.add(cart)
                x += 1
            track_sections.append(row)
            y += 1
        return Track(track_sections), carts


def find_collision(track, carts):
    cart_positions = set([cart.position for cart in carts])
    while True:
        ordered_carts = list(carts)
        ordered_carts.sort()
        for cart in ordered_carts:
            track_section = track.get(cart.position)
            if track_section.is_intersection:
                cart.turn()

            next_pos, next_direction = track_section.leads_to(cart.direction)
            if next_pos in cart_positions:
                return next_pos
            cart_positions.remove(cart.position)
            cart.move(next_pos, next_direction)
            cart_positions.add(next_pos)


def remove_colliding(track, carts):
    cart_positions = {cart.position: cart for cart in carts}
    while True:
        ordered_carts = list(carts)
        ordered_carts.sort()

        # Only one cart left
        if len(carts) == 1:
            return carts.pop().position

        for cart in ordered_carts:
            old_pos = cart.position
            if old_pos not in cart_positions:
                # This cart has collided, skip it
                continue

            track_section = track.get(old_pos)
            if track_section.is_intersection:
                cart.turn()

            next_pos, next_direction = track_section.leads_to(cart.direction)
            if next_pos in cart_positions:
                other_cart = cart_positions[next_pos]

                carts.remove(cart)
                carts.remove(other_cart)

                del cart_positions[next_pos]
            else:
                cart.move(next_pos, next_direction)
                cart_positions[next_pos] = cart

            del cart_positions[old_pos]



def main():
    import sys
    if len(sys.argv) < 2:
        print("usage: {} FILENAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    from copy import deepcopy

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    track, carts = read_file(filename)
    print("13.1 result: {}".format(find_collision(track, deepcopy(carts))))
    print("13.2 result: {}".format(remove_colliding(track, deepcopy(carts))))

if __name__ == '__main__':
    main()

