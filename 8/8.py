class Node():
    def __init__(self, values):
        self._no_children = int(values.pop(0))
        self._no_metadata_entries = int(values.pop(0))

        self._children = []
        for _ in range(0, self._no_children):
            self._children.append(Node(values))

        self._metadata = []
        for _ in range(0, self._no_metadata_entries):
            self._metadata.append((int(values.pop(0))))

    def sum_metadata(self):
        s = 0
        for child in self._children:
            s += child.sum_metadata()

        for md in self._metadata:
            s += md

        return s

    def value(self):
        s = 0
        if not self._children:
            for md in self._metadata:
                s += md
            return s
        else:
            for md in self._metadata:
                try:
                    s += self._children[md - 1].value()
                except IndexError:
                    pass
            return s


def read_file(filename):
    with open(filename, 'r') as f:
        values = f.read().split(' ')
    return Node(values)

def main():
    import sys
    if len(sys.argv) < 2:
        print("usage: {} FILENAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    tree = read_file(filename)
    print("8.1 result: {}".format(tree.sum_metadata()))
    print("8.2 result: {}".format(tree.value()))

if __name__ == '__main__':
    main()

