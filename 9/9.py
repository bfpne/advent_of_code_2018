def read_file(filename):
    with open(filename, 'r') as f:
        words = f.read().split(' ')
    return int(words[0]), int(words[6])

class Marble():
    def __init__(self, value, previous = None, next = None):
        self._value = value
        self._previous = None
        self._next = next

    @property
    def prev(self):
        return self._previous

    @prev.setter
    def prev(self, previous):
        self._previous = previous

    @property
    def next(self):
        return self._next

    @next.setter
    def next(self, next):
        self._next = next

    @property
    def value(self):
        return self._value

    def __str__(self):
        return str(self.value)


class Game():
    def __init__(self):
        self._current_marble = Marble(0)
        self._current_marble.prev = self._current_marble
        self._current_marble.next = self._current_marble
        self._current_value = 1

    def _place_marble(self, marble):
        if marble.value % 23 == 0:
            seven_marbles_back = self._current_marble.prev.prev.prev.prev.prev.prev.prev
            seven_marbles_back.prev.next = seven_marbles_back.next
            seven_marbles_back.next.prev = seven_marbles_back.prev
            self._current_marble = seven_marbles_back.next
            return marble.value + seven_marbles_back.value
        else:
            prev = self._current_marble.next
            next = prev.next

            prev.next = marble

            marble.prev = prev
            marble.next = next

            next.prev = marble
            self._current_marble = marble
            return 0

    def place_next_marble(self):
        score = self._place_marble(Marble(self._current_value))
        self._current_value += 1
        return self._current_value - 1, score


def calculate_high_score(players, last_marble_worth):
    game = Game()
    points = [0] * players
    while True:
        for player in range(0, players):
            marble_worth, score = game.place_next_marble()
            points[player] += score
            if marble_worth == last_marble_worth:
                return max(points)


def main():
    import sys
    if len(sys.argv) < 2:
        print("usage: {} FILENAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    players, last_marble_worth = read_file(filename)
    print("9.1 result: {}".format(calculate_high_score(players, last_marble_worth)))
    print("9.1 result: {}".format(calculate_high_score(players, last_marble_worth * 100)))

if __name__ == '__main__':
    main()

