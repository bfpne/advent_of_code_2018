def read_list(filename):
    l = []
    with open(filename, 'r') as f:
        for change in f.readlines():
            l.append(int(change.strip()))
    return l

def calibrate(filename):
    current_value = 0
    values_seen = set()
    frequency_list = read_list(filename)
    while True:
        for change in frequency_list:
            values_seen.add(current_value)
            current_value += change
            # Already seen once, now seen twice
            if current_value in values_seen:
                return current_value
    

def frequency(filename):
    current_value = 0
    with open(filename, 'r') as f:
        for line in f.readlines():
            current_value += int(line.strip())
            # Already seen once, now seen twice
    return current_value

def main():
    import sys
    if len(sys.argv) == 1:
        print('usage: {} INPUT'.format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[1]
    print("Reading from '{}'".format(filename))
    print('1.1 result: {}'.format(frequency(filename)))
    print('1.2 result: {}'.format(calibrate(filename)))

if __name__ == '__main__':
    main()
