class Point():
    def __init__(self, x, y, vx, vy):
        self._x = x
        self._y = y
        self._vx = vx
        self._vy = vy

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    def move(self):
        self._x += self._vx
        self._y += self._vy
        return self._x, self._y

def parse_point(line):
    import re
    result = re.search('position=\< *(-?[0-9]+), *(-?[0-9]+)\> velocity=\< *(-?[0-9]+), *(-?[0-9]+)\>', line)
    return Point(int(result.group(1)), int(result.group(2)), int(result.group(3)), int(result.group(4)))


def read_file(filename):
    points = set()
    with open(filename, 'r') as f:
        for l in f.readlines():
            points.add(parse_point(l.strip()))
    return points

def print_positions(positions):
    min_x = min(positions, key=lambda p: p[0])[0]
    max_x = max(positions, key=lambda p: p[0])[0]
    min_y = min(positions, key=lambda p: p[1])[1]
    max_y = max(positions, key=lambda p: p[1])[1]

    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            if (x, y) in positions:
                print('#', end='')
            else:
                print('.', end='')
        print()


    return (min_x, max_x, min_y, max_y)

def move_points(points):
    min_x = 0
    max_x = 0
    positions = set()
    for point in points:
        x, y = point.move()
        if x < min_x:
            min_x = x
        if x > max_x:
            max_x = x
        positions.add((x, y))

    return positions, max_x - min_x

def find_text(points):
    prev_positions, prev_width = move_points(points)
    moves = 1

    while True:
        positions, width = move_points(points)
        if width > prev_width:
            print_positions(prev_positions)
            return moves
        else:
            moves += 1
            prev_width = width
            prev_positions = positions


def main():
    import sys
    if len(sys.argv) < 2:
        print("usage: {} FILENAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    points = read_file(filename)
    print("10.2 result: {}".format(find_text(points)))

if __name__ == '__main__':
    main()

