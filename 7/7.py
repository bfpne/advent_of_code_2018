def read_file(filename):
    import re
    rules = []
    with open(filename, 'r') as f:
        for l in f.readlines():
            result = re.search('Step ([A-Z]) must be finished before step ([A-Z]) can begin\.', l)
            rules.append((result.group(1), result.group(2)))
    return rules

class SortedSet():
    def __init__(self, initial_list):
        self._list = initial_list.copy()
        self._list.sort()

    def pop(self):
        return self._list.pop(0)

    def add(self, item):
        if item not in self._list:
            self._list.append(item)
            self._list.sort()

    def empty(self):
        return len(self._list) == 0

    def __str__(self):
        return str(self._list)

def assembly_order(rules):
    from collections import defaultdict
    depends_on = defaultdict(lambda: [])
    produces = defaultdict(lambda: [])
    all_steps = set()
    for x, y in rules:
        depends_on[y].append(x)
        produces[x].append(y)
        
        all_steps.add(x)
        all_steps.add(y)

    available_steps = SortedSet([step for step in all_steps if step not in depends_on])

    completed_steps = []
    while True:
        try:
            current_step = available_steps.pop()
            completed_steps.append(current_step)
        except IndexError:  # available_steps is empty
            return ''.join(completed_steps)

        def all_dependants_are_completed(step):
            for depends in depends_on[step]:
                if depends not in completed_steps:
                    return False

            return True

        for step in produces[current_step]:
            if all_dependants_are_completed(step):
                available_steps.add(step)


def assembly_time(rules):
    from collections import defaultdict
    depends_on = defaultdict(lambda: [])
    produces = defaultdict(lambda: [])
    all_steps = set()
    for x, y in rules:
        depends_on[y].append(x)
        produces[x].append(y)
        
        all_steps.add(x)
        all_steps.add(y)

    available_steps = SortedSet([step for step in all_steps if step not in depends_on])

    current_time = 0
    steps_in_progress = []
    completed_steps = []

    while True:
        # Make sure everyone works!
        while len(steps_in_progress) < 5 and not available_steps.empty():
            step = available_steps.pop()
            steps_in_progress.append((step, current_time + 60 + ord(step) - 64))

        # As many workers as possible are busy, sort their work on first completion time
        steps_in_progress.sort(key=lambda x: x[1])

        # Pop the fist finished step
        finished_step, finished_at = steps_in_progress.pop(0)
        current_time = finished_at
        completed_steps.append(finished_step)

        def all_dependants_are_completed(step):
            for depends in depends_on[step]:
                if depends not in completed_steps:
                    return False

            return True

        for step in produces[finished_step]:
            if all_dependants_are_completed(step):
                available_steps.add(step)

        if not steps_in_progress and available_steps.empty():
            return current_time


def main():
    import sys
    if len(sys.argv) < 2:
        print("usage: {} FILENAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    rules = read_file(filename)
    print("7.1 result: {}".format(assembly_order(rules)))
    print("7.2 result: {}s".format(assembly_time(rules)))

if __name__ == '__main__':
    main()

