from datetime import datetime
from collections import defaultdict
import re

def parse(line):
    r = re.search('\[([0-9]+)-([0-9]+)-([0-9]+) ([0-9]{2}):([0-9]{2})\] (.+$)', line)

    year = int(r.group(1))
    month = int(r.group(2))
    day = int(r.group(3))

    hour = int(r.group(4))
    minute = int(r.group(5))

    action = r.group(6)

    return (datetime(year, month, day, hour, minute), action)


def read_file(filename):
    entries = []
    with open(filename, 'r') as f:
        for l in f.readlines():
            entries.append(parse(l.strip()))
    entries.sort(key=lambda x: x[0])
    return entries


def strategy_1(entries):
    guard_data = defaultdict(lambda: [])

    guard_id = None
    fell_asleep = None
    minutes_asleep_during_shift = []

    for date, action in entries:
        if action.startswith('Guard'):  # Shift begins
            if guard_id:
                guard_data[guard_id].extend(minutes_asleep_during_shift) 

            # Check who starts
            id_start = action.find('#')
            id_end = action.find(' ', id_start)
            guard_id = action[id_start:id_end]
            
            # Reset shift data
            fell_asleep = None
            minutes_asleep_during_shift = []

        elif action == 'falls asleep':
            fell_asleep = date.minute

        elif action == 'wakes up':
            for minute in range(fell_asleep, date.minute):
                minutes_asleep_during_shift.append(minute)
        else:
            raise RuntimeError('Uknown action ' + action )

    guard_that_sleeps_most = None
    most_minutes_slept = 0
    for guard_id, minutes in guard_data.items():
        minutes_slept = len(minutes)
        if minutes_slept > most_minutes_slept:
            guard_that_sleeps_most = guard_id
            most_minutes_slept = minutes_slept 

    minute_list = guard_data[guard_that_sleeps_most]
    minutes = defaultdict(lambda: 0)

    for minute in minute_list:
        minutes[minute] += 1

    minute_most_often_asleep = None
    most_times_asleep = 0
    for minute, times_asleep in minutes.items():
        if times_asleep > most_times_asleep:
            minute_most_often_asleep = minute
            most_times_asleep = times_asleep

    return int(guard_that_sleeps_most[1:]) * minute_most_often_asleep

def strategy_2(entries):
    guard_data = defaultdict(lambda: [])

    guard_id = None
    fell_asleep = None
    minutes_asleep_during_shift = []

    for date, action in entries:
        if action.startswith('Guard'):  # Shift begins
            if guard_id:
                guard_data[guard_id].extend(minutes_asleep_during_shift) 

            # Check who starts
            id_start = action.find('#')
            id_end = action.find(' ', id_start)
            guard_id = action[id_start:id_end]
            
            # Reset shift data
            fell_asleep = None
            minutes_asleep_during_shift = []

        elif action == 'falls asleep':
            fell_asleep = date.minute

        elif action == 'wakes up':
            for minute in range(fell_asleep, date.minute):
                minutes_asleep_during_shift.append(minute)
        else:
            raise RuntimeError('Uknown action ' + action )

    sleep_data = []
    for guard_id, minute_list in guard_data.items():
        minutes = defaultdict(lambda: 0)

        for minute in minute_list:
            minutes[minute] += 1

        minute_most_often_asleep = None
        most_times_asleep = 0
        for minute, times_asleep in minutes.items():
            if times_asleep > most_times_asleep:
                minute_most_often_asleep = minute
                most_times_asleep = times_asleep

        sleep_data.append((guard_id, most_times_asleep, minute_most_often_asleep))

    sleep_data.sort(key=lambda x: x[1])
    guard_id, times, minute = sleep_data.pop()
    return int(guard_id[1:]) * minute


def main():
    import sys
    if len(sys.argv) < 2:
        print("usage: {} FILENAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)

    filename = sys.argv[1]
    print("Reading from: {}".format(filename))
    entries = read_file(filename)
    print("4.1 result: {}".format(strategy_1(entries)))
    print("4.2 result: {}".format(strategy_2(entries)))

if __name__ == '__main__':
    main()

